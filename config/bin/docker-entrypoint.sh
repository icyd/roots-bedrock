#!/bin/bash
set -euo pipefail

USER="nginx"
GROUP="nginx"

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	if [ "${!var:-}" ]; then
		def="${!var}"
	elif [ "${!fileVar:-}" ]; then
		def="$(< "${!fileVar}")"
	fi
	export "$var"="$def"
	unset "$fileVar"
}

# Escape right hand argument for sed
sed_escape() {
	echo "$@" | sed -e 's/[\/&]/\\&/g'
}

# Change configuration values on file
set_config() {
	key="$1"
	value="$2"
	sed -ri 's/^(#\s*)?('"$key"'=).*$/\2'"\'$(sed_escape ${value})\'"'/' .env
}

if [ ! -e composer.lock ] && [ ! -e web/index.php ]; then

	echo >&2 "WordPress not found in $PWD - copying now..."
	if [ -n "$(ls -A)" ]; then
		echo >&2 "WARNING: $PWD is not empty! (copying anyhow)"
	fi
	sourceTarArgs=(
		--create
		--file -
		--directory /usr/src/bedrock
		--owner "$USER" --group "$GROUP"
	)
	targetTarArgs=(
		--extract
		--file -
		--no-overwrite-dir
	)
	tar "${sourceTarArgs[@]}" . | tar "${targetTarArgs[@]}"
	echo >&2 "Complete! WordPress has been successfully copied to $PWD"
fi

uniqueEnvs=(
	AUTH_KEY
	SECURE_AUTH_KEY
	LOGGED_IN_KEY
	NONCE_KEY
	AUTH_SALT
	SECURE_AUTH_SALT
	LOGGED_IN_SALT
	NONCE_SALT
)
envs=(
	WORDPRESS_DB_HOST
	WORDPRESS_DB_USER
	WORDPRESS_DB_PASSWORD
	WORDPRESS_DB_NAME
	WORDPRESS_DB_CHARSET
	WORDPRESS_DB_COLLATE
	"${uniqueEnvs[@]/#/WORDPRESS_}"
	WORDPRESS_TABLE_PREFIX
	WORDPRESS_CONFIG_EXTRA
	WORDPRESS_ENV
	WORDPRESS_HOME
	WORDPRESS_DEBUG_LOG
)

# Check if environment variables are given and process FILES
for e in "${envs[@]}"; do
	file_env "$e"
done

: "${WORDPRESS_DB_HOST:=127.0.0.1}"
: "${WORDPRESS_DB_USER:=user}"
: "${WORDPRESS_DB_PASSWORD:=1234}"
: "${WORDPRESS_DB_NAME:=wordpress}"
: "${WORDPRESS_DB_CHARSET:=utf8mb4}"
: "${WORDPRESS_DB_COLLATE:=}"
: "${WORDPRESS_TABLE_PREFIX:=wp_}"
: "${WORDPRESS_ENV:=development}"
: "${WORDPRESS_HOME:=bedrock.docker}"
: "${WORDPRESS_DEBUG_LOG:=debug.log}"

if [ "$WORDPRESS_ENV" == "development" ]; then
	# Enable PHP Xdebug module
	[ -z "$(php -m | grep 'xdebug')" ] \
	&& docker-php-ext-enable xdebug
	# Run php as root
    sed -ri 's/(^(user|group)\s+=\s+).*/\1root/' /usr/local/etc/php-fpm.d/www.conf
	sed -ri 's/^command.*?php-fpm.*/& -R/' /etc/supervisor/supervisord.conf
	sed -ri 's/^\s*(#\s*)?user\s+nginx;$/#&/' /etc/nginx/nginx.conf
	sed -ri 's!^(\s*)(include\s+snippets/security.*)!\1# \2!' /etc/nginx/conf.d/default.conf
	chown -R root:root /var/www/html
else
	# Enable PHP redis module
	[ -z "$(php -m | grep 'redis')" ] \
	&& docker-php-ext-enable redis
    sed -ri 's/(^(user|group)\s+=\s+).*/\1nginx/' /usr/local/etc/php-fpm.d/www.conf
	sed -ri 's/^\s*#\s*(user\s*nginx;)$/\1/' /etc/nginx/nginx.conf
fi

set_config 'DB_NAME' "$WORDPRESS_DB_NAME"
set_config 'DB_USER' "$WORDPRESS_DB_USER"
set_config 'DB_PASSWORD' "$WORDPRESS_DB_PASSWORD"
set_config 'DB_HOST' "$WORDPRESS_DB_HOST"
set_config 'DB_PREFIX' "$WORDPRESS_TABLE_PREFIX"
set_config 'WP_ENV' "$WORDPRESS_ENV"
set_config 'WP_HOME' "$WORDPRESS_HOME"
set_config 'WP_DEBUG_LOG' "$WORDPRESS_DEBUG_LOG"

for unique in "${uniqueEnvs[@]}"; do
	uniqVar="WORDPRESS_$unique"
	if [ -n "${!uniqVar}" ]; then
		set_config "$unique" "${!uniqVar}"
	else
		currentVal=$(sed -rn "s/^${unique}='(.*?)'\$/\\1/p" .env)
		if [ "$currentVal" = 'generateme' ]; then
			set_config "$unique" "$(head -c1m /dev/urandom | sha1sum | cut -d' ' -f1)"
		fi
	fi
done

for unique in "DB_COLLATE" "DB_CHARSET"; do
	uniqVar="WORDPRESS_$unique"
	if [ -z "$(grep "${unique}" .env)" ]; then
		set_config "$unique" "${!uniqVar}"
	else
		sed -i "/DB_PREFIX/a ${unique}='${!uniqVar}'" .env
	fi
	sed -ri "s/^(Config::define\\('${unique}',\\s)'\\w*'(.*)$/\\1env\\('${unique}'\\)\\2/" config/application.php
done

for e in "${envs[@]}"; do
	unset "$e"
done

exec "$@"
