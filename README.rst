roots-bedrock
=============

Container image for roots' Bedrock

It uses as reference `WordPress container image <https://github.com/docker-library/wordpress>`_, but using the installation of `Roots Bedrock <https://roots.io/bedrock/>`_. This images contain two process: *php-fpm* and *nginx* manage with *supervisor*. It allows to modify the configuration with environment variables.

Environment variables
--------------------

- WORDPRESS_DB_HOST
- WORDPRESS_DB_USER
- WORDPRESS_DB_PASSWORD
- WORDPRESS_DB_NAME
- WORDPRESS_DB_CHARSET
- WORDPRESS_DB_COLLATE
- WORDPRESS_DB_URL **(not yet implemented)**
- WORDPRESS_AUTH_KEY
- WORDPRESS_SECURE_AUTH_KEY
- WORDPRESS_LOGGED_IN_KEY
- WORDPRESS_NONCE_KEY
- WORDPRESS_AUTH_SALT
- WORDPRESS_SECURE_AUTH_SALT
- WORDPRESS_LOGGED_IN_SALT
- WORDPRESS_NONCE_SALT
- WORDPRESS_TABLE_PREFIX
- WORDPRESS_CONFIG_EXTRA **(not yet implemented)**
- WORDPRESS_ENV
- WORDPRESS_HOME
- WORDPRESS_DEBUG_LOG


**NOTE:** At difference of the WordPress image, this don't automatically creates the database, in order to simplify the `docker-entrypoint.sh` file and speed up the creation of the container.

Features
--------

- php-fpm.
- nginx.
- ghostscript.
- Gzip & brotli compression.
- Expire headers.
- Security headers.
- Subdomain and subfolder multisite (not active by default).
- Runs proccess as user `nginx`.
- php modules: gd mysqli exif sockets opcache intl bcmath zip ssh2 gmagick mcrypt redis (enabled on production and staging) xdebug (enabled on development).
- On development mode, processes and working directory are run and owned by root. This allows local development with bind volumen and podman (rootless), without conflict of permissions.
