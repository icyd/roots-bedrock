ARG NGINX_VERSION=1.17.10
ARG PKG_RELEASE=1~buster

FROM nginx:${NGINX_VERSION} AS builder

ARG NGINX_VERSION
ARG PKG_RELEASE

RUN set -eux; \
    apt update -y \
    && apt install -y --no-install-recommends --no-install-suggests \
    build-essential \
    curl \
    git \
    ca-certificates \
    && cd /tmp \
    && curl -O https://hg.nginx.org/pkg-oss/raw-file/default/build_module.sh \
    && sed -ir 's/sudo\s*//' ./build_module.sh \
    && git clone https://github.com/google/ngx_brotli.git \
    && cd ngx_brotli \
    && git submodule update --init --recursive \
    && cd /tmp \
    && sh ./build_module.sh --non-interactive --force-dynamic -v ${NGINX_VERSION} ./ngx_brotli \
    && pkg=$(find /root/debuild/nginx-${NGINX_VERSION}/debian/debuild-module-brotli -type f -name "nginx-module-brotli_${NGINX_VERSION}-${PKG_RELEASE}*.deb" -print) \
    && dpkg -i ${pkg}


FROM php:7.2.30-fpm-buster

ARG NGINX_VERSION
ARG PKG_RELEASE
ENV BEDROCK_VERSION  1.13.4
ENV COMPOSER_VERSION 1.10.6
ENV NJS_VERSION      0.3.9
ENV NGINX_GPGKEY     573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62


# Installation of php-modules and nginx
RUN set -eux; \
    addgroup --system --gid 101 nginx \
    && adduser --system --disabled-login --ingroup nginx \
        --no-create-home --home /var/www/html \
        --gecos "nginx user" --shell /bin/bash --uid 101 nginx; \
    savedAptMark="$(apt-mark showmanual)"; \
    apt update -y \
    && apt install -y --no-install-recommends --no-install-suggests \
        gnupg1 ca-certificates \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libgraphicsmagick1-dev \
        libssh2-1-dev \
        libmcrypt-dev \
        libicu-dev \
        libzip-dev \
        zlib1g-dev \
    # reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    && apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark \
    && \
        found=''; \
        for server in \
            ha.pool.sks-keyservers.net \
            hkp://keyserver.ubuntu.com:80 \
            hkp://p80.pool.sks-keyservers.net:80 \
            pgp.mit.edu \
        ; do \
            echo "Fetching GPG key ${NGINX_GPGKEY} from $server"; \
            apt-key adv --keyserver "$server" --keyserver-options timeout=10 \
            --recv-keys "${NGINX_GPGKEY}" && found=yes && break; \
        done; \
        test -n "$found" \
    && yes '' | pecl install ssh2-1.2 mcrypt-1.0.3 gmagick-2.0.5RC1 xdebug-2.9.5 redis-5.2.2 \
    && docker-php-ext-configure gd \
        --with-freetype-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
    && docker-php-ext-install -j"$(nproc)" \
        gd \
        mysqli \
        exif \
        sockets \
        opcache \
        intl \
        bcmath \
        zip \
    && docker-php-ext-enable ssh2 gmagick mcrypt; \
    dpkgArch="$(dpkg --print-architecture)" \
    && nginxPackages=" \
        nginx=${NGINX_VERSION}-${PKG_RELEASE} \
        nginx-module-xslt=${NGINX_VERSION}-${PKG_RELEASE} \
        nginx-module-geoip=${NGINX_VERSION}-${PKG_RELEASE} \
        nginx-module-image-filter=${NGINX_VERSION}-${PKG_RELEASE} \
        nginx-module-njs=${NGINX_VERSION}.${NJS_VERSION}-${PKG_RELEASE} \
    " \
    && case "$dpkgArch" in \
        amd64|i386) \
            echo "deb https://nginx.org/packages/mainline/debian/ buster nginx" >> /etc/apt/sources.list.d/nginx.list \
            && apt-get update -y \
            ;; \
        *) \
            echo >&2 "error: Only i386 & amd64 supported" && exit 1 \
            ;;\
        esac; \
    apt-get install --no-install-recommends --no-install-suggests -y \
        $nginxPackages \
        gettext-base; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
        | awk '/=>/ { print $3 }' \
        | sort -u \
        | xargs -r dpkg-query -S \
        | cut -d: -f1 \
        | sort -u \
        | xargs -rt apt-mark manual; \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /tmp/pear /var/lib/apt/lists/* /etc/apt/sources.list.d/nginx.list

    COPY --from=builder /usr/lib/nginx/modules/* /usr/lib/nginx/modules/

# Install other packages
RUN set -eux; \
    apt update -y \
    && apt install -y --no-install-recommends --no-install-suggests \
        supervisor \
        curl \
        ca-certificates \
        ghostscript \
    && EXPECTED_CHECKSUM="$(curl -s https://composer.github.io/installer.sig)" \
    && curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php \
    && [ "$EXPECTED_CHECKSUM" = "$(php -r "echo hash_file('sha384', '/tmp/composer-setup.php');")" ] || exit 1; \
    php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer --version="${COMPOSER_VERSION}" \
    && apt-get remove --purge --auto-remove -y curl \
    && rm -rf /tmp/composer-setup.php /var/lib/apt/lists/*; \
    mkdir -p /usr/src/bedrock \
    && chmod 755 /var/www/html \
    && chown nginx:nginx /var/www/html /usr/src/bedrock \
    && su -c "composer create-project roots/bedrock=${BEDROCK_VERSION} /usr/src/bedrock" - nginx

EXPOSE 80
WORKDIR /var/www/html

# Copying configuration files
COPY config /tmp/config
RUN set -eux; \
    chmod +x /tmp/config/bin/*; \
    mv /tmp/config/bin/* /usr/local/bin/; \
    mv /tmp/config/nginx/nginx.conf /etc/nginx/nginx.conf; \
    mv /tmp/config/nginx/conf.d/* /etc/nginx/conf.d/; \
    mv /tmp/config/nginx/snippets /etc/nginx/; \
    mv /tmp/config/php/conf.d/* /usr/local/etc/php/conf.d/; \
    mv /tmp/config/supervisor/supervisord.conf /etc/supervisor/supervisord.conf; \
    rm -rf /tmp/config

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
